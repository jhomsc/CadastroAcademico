﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CadastroAcademico.Models
{
    public class Job
    {
        [Key]
        public int Id { get; set; }

        [Required(ErrorMessage = "The field {0} is required")]
        [StringLength(100, ErrorMessage = "The field {0} can contain maximun {1} and minimun {2} characters", MinimumLength = 5)]
        [Display(Name = "Nombre de la tarea")]
        public string Name { get; set; }

        [Required(ErrorMessage = "The field {0} is required")]
        [StringLength(1000, ErrorMessage = "The field {0} can contain maximun {1} and minimun {2} characters", MinimumLength = 5)]
        [Display(Name = "Descripción de la tarea")]
        public string Description { get; set; }

        [Required(ErrorMessage = "The field {0} is required")]
        [Display(Name = "Fecha de inicio")]
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime BeginDate { get; set; }

        [Required(ErrorMessage = "The field {0} is required")]
        [Display(Name = "Fecha final")]
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime EndDate { get; set; }

        [Display(Name = "Materia")]
        public int MatterId { get; set; }

        public TypeTask TypeTask { get; set; }

        public virtual Matter Matter { get; set; }
        public virtual ICollection<UserJob> UserJobs { get; set; }

    }
}