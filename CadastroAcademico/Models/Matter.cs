﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CadastroAcademico.Models
{
    public class Matter
    {
        [Key]
        public int Id { get; set; }

        [Required(ErrorMessage = "The field {0} is required")]
        [StringLength(100, ErrorMessage = "The field {0} can contain maximun {1} and minimun {2} characters", MinimumLength = 5)]
        [Display(Name = "Nombre de la materia")]
        public string Name { get; set; }

        [Required(ErrorMessage = "The field {0} is required")]
        [StringLength(1000, ErrorMessage = "The field {0} can contain maximun {1} and minimun {2} characters", MinimumLength = 5)]
        [Display(Name = "Descripción de la materia")]
        public string Description { get; set; }

        [Display(Name = "Professor")]
        public int UserId { get; set; }

        [Display(Name = "Curso")]
        public int CourseId { get; set; }

        public virtual User User { get; set; }
        public virtual Course Course { get; set; }
        public virtual ICollection<Job> Jobs { get; set; }
    }
}