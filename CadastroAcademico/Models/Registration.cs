﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CadastroAcademico.Models
{
    public class Registration
    {
        [Key]
        public int Id { get; set; }

        [Required(ErrorMessage = "The field {0} is required")]
        [Display(Name = "Fecha de registro")]
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime RegistrationDate { get; set; }

        [Display(Name = "Curso")]
        public int CourseId { get; set; }

        [Display(Name = "Usuario")]
        public int UserId { get; set; }

        public virtual User User { get; set; }
        public virtual Course Course { get; set; }

    }
}