﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CadastroAcademico.Models
{
    public class UserJob
    {
        [Key]
        public int Id { get; set; }

        [Required(ErrorMessage = "The field {0} is required")]
        [Display(Name = "File")]
        [DataType(DataType.Url)]
        public string file { get; set; }

        [Required(ErrorMessage = "The field {0} is required")]
        [Display(Name = "Fecha de registro")]
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime RegistrationDate { get; set; }

        [Display(Name = "Tarea")]
        public int JobId { get; set; }

        [Display(Name = "Usuario")]
        public int UserId { get; set; }

        public virtual User User { get; set; }
        public virtual Job Job { get; set; }
    }
}