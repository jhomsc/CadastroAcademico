﻿using CadastroAcademico.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace CadastroAcademico
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            this.CreateBD();
            this.CreateUser("admin@unip.com", TypeUser.Administrador, "Administrador", "Aplicacion");
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }


        private void CreateBD()
        {
            var userContext = new ApplicationDbContext();
            var db = new CadastroAcademicoContext();

            this.CheckRole(TypeUser.Professor.ToString(), userContext);
            this.CheckRole(TypeUser.Usuario.ToString(), userContext);


            var user = db.Users.FirstOrDefault();
            var course = db.Courses.FirstOrDefault();

            if (user == null && course == null)
            {
                // crearemos los usuarios de Profesores y Alumnos

                this.CreateUser("professor@unip.com", TypeUser.Professor, "Renata", "De Oliveira");
                this.CreateUser("aluno@unip.com", TypeUser.Usuario, "Emerson", "Silva");
                this.CreateUser("alunomark@unip.com", TypeUser.Usuario, "aluno1", "Marketing");
                

                //Crear un curso

                this.CreateCourse(db,"Análise de Sistemas", "Este curso engloba conhecimentos nas áreas de Sistemas Operacionais, Redes, Segurança da Informação, Engenharia de Software e Desenvolvimento de aplicação Web.", true);

                this.CreateMatter(db, "Análise de Sistemas", "professor@unip.com", "Introdução a Computação (AS)", "Conhecimento da informática, histórico e conceitos. Sistema de Numeração. Representação da Escrita no Computador. Noções de Informática. Arquitetura de um computador etc.");

                    this.CreatJob(db, "Introdução a Computação (AS)", "AS_DPs", "Esta é la DP do curso", TypeTask.DPs, DateTime.Now.AddMonths(-1), DateTime.Now.AddMonths(1));
                    this.CreatJob(db, "Introdução a Computação (AS)", "AS_PIM", "Este PIM é de caráter obrigatório", TypeTask.PIM, DateTime.Now.AddMonths(-2), DateTime.Now.AddMonths(-1));
                    this.CreatJob(db, "Introdução a Computação (AS)", "AS_Trabalho", "Todos os trabalhos devem estar relacionados com las materias do curso", TypeTask.Trabalhos, DateTime.Now.AddMonths(-2), DateTime.Now.AddMonths(-1));


               this.CreateMatter(db, "Análise de Sistemas", "professor@unip.com", "Programação Orientada a Objeto (AS)", "Princípios da Orientação a Objeto, Classes, Objetos, Atributos, Métodos, Métodos, Construtores e Destrutores, Modificadores de Acesso, Herança, Polimorfismo, Templates.");

                    this.CreatJob(db, "Programação Orientada a Objeto (AS)", "AS_DPs", "Esta é la DP do curso", TypeTask.DPs, DateTime.Now.AddMonths(-1), DateTime.Now.AddMonths(1));
                    this.CreatJob(db, "Programação Orientada a Objeto (AS)", "AS_PIM", "Este PIM é de caráter obrigatório", TypeTask.PIM, DateTime.Now.AddMonths(-2), DateTime.Now.AddMonths(-1));
                    this.CreatJob(db, "Programação Orientada a Objeto (AS)", "AS_Trabalho", "Todos os trabalhos devem estar relacionados com las materias do curso", TypeTask.Trabalhos, DateTime.Now.AddMonths(-2), DateTime.Now.AddMonths(-1));

                
                this.CreateMatter(db, "Análise de Sistemas", "professor@unip.com", "Português (AS)", "Comunicação e linguagem, variação linguística. Leitura, texto coerência, coesão e textualidade, correção gramatical. Leitura, interpretação e produção de textos.");

                    this.CreatJob(db, "Português (AS)", "AS_DPs", "Esta é la DP do curso", TypeTask.DPs, DateTime.Now.AddMonths(-1), DateTime.Now.AddMonths(1));
                    this.CreatJob(db, "Português (AS)", "AS_PIM", "Este PIM é de caráter obrigatório", TypeTask.PIM, DateTime.Now.AddMonths(-2), DateTime.Now.AddMonths(-1));
                    this.CreatJob(db, "Português (AS)", "AS_Trabalho", "Todos os trabalhos devem estar relacionados com las materias do curso", TypeTask.Trabalhos, DateTime.Now.AddMonths(-2), DateTime.Now.AddMonths(-1));


                this.CreateCourse(db, "Administração em Marketing", "Administração: gestão, habilidades, níveis, escolas. Marketing e Publicidade: definições, análise do consumidor e a importância do meio ambiente, segmentação e nomenclaturas mais usadas.", true);
                this.CreateMatter(db, "Administração em Marketing", "professor@unip.com", "Estatística (AM)", "Apresentação de dados em tabelas, medidas de tendência central para uma amostra, medidas de dispersão, desvio-padrão, variância e coeficiente de variação.");

                    this.CreatJob(db, "Estatística (AM)", "AS_DPs", "Esta é la DP do curso", TypeTask.DPs, DateTime.Now.AddMonths(-1), DateTime.Now.AddMonths(1));
                    this.CreatJob(db, "Estatística (AM)", "AS_PIM", "Este PIM é de caráter obrigatório", TypeTask.PIM, DateTime.Now.AddMonths(-2), DateTime.Now.AddMonths(-1));
                    this.CreatJob(db, "Estatística (AM)", "AS_Trabalho", "Todos os trabalhos devem estar relacionados com las materias do curso", TypeTask.Trabalhos, DateTime.Now.AddMonths(-2), DateTime.Now.AddMonths(-1));


                this.CreateMatter(db, "Administração em Marketing", "professor@unip.com", "Fotografia (AM)", "Processos técnicos para a produção de imagens fotográficas. Operação de câmeras analógicas, digitais e acessórios.");

                    this.CreatJob(db, "Fotografia (AM)", "AS_DPs", "Esta é la DP do curso", TypeTask.DPs, DateTime.Now.AddMonths(-1), DateTime.Now.AddMonths(1));
                    this.CreatJob(db, "Fotografia (AM)", "AS_PIM", "Este PIM é de caráter obrigatório", TypeTask.PIM, DateTime.Now.AddMonths(-2), DateTime.Now.AddMonths(-1));
             
                ////Registro de usuarios
                this.CreateRegistration(db, "aluno@unip.com", "Análise de Sistemas");
                this.CreateRegistration(db, "alunomark@unip.com", "Administração em Marketing");
            }

        }

        private void CreateRegistration(CadastroAcademicoContext db,string userName,  string courseName)
        {
            var course = db.Courses.Where(u => u.Name.Equals(courseName)).FirstOrDefault();
            var user = db.Users.Where(u => u.UserName.Equals(userName)).FirstOrDefault();

            var registration = new Registration
            {
                CourseId = course.Id,
                UserId = user.UserId,
                RegistrationDate=DateTime.Now,
            };
            db.Registrations.Add(registration);
            db.SaveChanges();
        }

        private void CreateUser( string email, TypeUser rol, string firstName, string lastName)
        {
            var userContext = new ApplicationDbContext();
            var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(userContext));
            var db = new CadastroAcademicoContext();

            this.CheckRole(rol.ToString(), userContext);

            var user = db.Users.Where(u => u.UserName.ToLower().Equals(email)).FirstOrDefault();

            if (user == null)
            {
                user = new User
                {
                    UserName = email,
                    FirstName = firstName,
                    LastName = lastName,
                    TypeUser = rol,
                };
                db.Users.Add(user);
                db.SaveChanges();
            }

            var userASP = userManager.FindByName(user.UserName);
            if (userASP == null)
            {
                userASP = new ApplicationUser
                {
                    UserName = user.UserName,
                    Email = user.UserName,
                };

                userManager.Create(userASP, userASP.UserName);
                userManager.AddToRole(userASP.Id, rol.ToString());
            }


        }

        private void CreateMatter(CadastroAcademicoContext db, string courseName ,string teacher, string name, string description)
        {
           
            var course = db.Courses.Where(u => u.Name.Equals(courseName)).FirstOrDefault();
            var user = db.Users.Where(u => u.UserName.Equals(teacher)).FirstOrDefault();

            var matter = new Matter
            {
                Name = name,
                Description = description,
                CourseId = course.Id,
                UserId = user.UserId,
            };
            db.Matters.Add(matter);
            db.SaveChanges();
        }

        private void CreatJob(CadastroAcademicoContext db, string matterName, string name, string description, TypeTask typeTask, DateTime beginDate, DateTime endDate)
        {

            var matter = db.Matters.Where(u => u.Name.Equals(matterName)).FirstOrDefault();

            var job = new Job
            {
                Name = name,
                Description = description,
                MatterId = matter.Id,
                TypeTask = typeTask,
                BeginDate = beginDate,
                EndDate = endDate,
            };
            db.Jobs.Add(job);
            db.SaveChanges();
        }

        private void CreateCourse(CadastroAcademicoContext db, string name, string description,bool active)
        {
           

            var course = new Course
            {
                Active = active,
                CreationDate = DateTime.Now,
                Name = name,
                Description = description,
            };
            db.Courses.Add(course);
            try
            {
                db.SaveChanges();

            }
            catch (Exception ex)
            {

                string error = ex.Message;
            }

        }

        private void CheckRole(string roleName,ApplicationDbContext userContext)
        {
            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(userContext));

            if (!roleManager.RoleExists(roleName))
            {
                roleManager.Create(new IdentityRole(roleName));
            }
        }
    }
}
