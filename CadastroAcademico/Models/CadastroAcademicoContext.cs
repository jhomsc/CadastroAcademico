﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Web;

namespace CadastroAcademico.Models
{
    public class CadastroAcademicoContext : DbContext
    {
        // You can add custom code to this file. Changes will not be overwritten.
        // 
        // If you want Entity Framework to drop and regenerate your database
        // automatically whenever you change your model schema, please use data migrations.
        // For more information refer to the documentation:
        // http://msdn.microsoft.com/en-us/data/jj591621.aspx
    
        public CadastroAcademicoContext() : base("name=CadastroAcademicoContext")
        {
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
        }

        public DbSet<Job> Jobs { get; set; }
        public DbSet<Course> Courses { get; set; }
        public DbSet<Matter> Matters { get; set; }
        public DbSet<Registration> Registrations { get; set; }
        public DbSet<UserJob> UserJobs { get; set; }
        public DbSet<User> Users { get; set; }
    }
}
