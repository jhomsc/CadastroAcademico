﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CadastroAcademico.Models
{
    public class UserJobManagerView
    {
        
        public int JobId { get; set; }

        [StringLength(100, ErrorMessage = "The field {0} can contain maximun {1} and minimun {2} characters", MinimumLength = 5)]
        [Display(Name = "Nombre del Curso")]
        public string CourseName { get; set; }

        [StringLength(100, ErrorMessage = "The field {0} can contain maximun {1} and minimun {2} characters", MinimumLength = 5)]
        [Display(Name = "Nombre de la materia")]
        public string MatterName { get; set; }

        [StringLength(100, ErrorMessage = "The field {0} can contain maximun {1} and minimun {2} characters", MinimumLength = 5)]
        [Display(Name = "Nombre de la tarea")]
        public string JobName { get; set; }

        [StringLength(100, ErrorMessage = "The field {0} can contain maximun {1} and minimun {2} characters", MinimumLength = 5)]
        [Display(Name = "Descripción de la tarea")]
        public string JobDescription { get; set; }

        [Display(Name = "Fecha de inicio")]
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime JobBeginDate { get; set; }

        [Display(Name = "Fecha final")]
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime JobEndDate { get; set; }
       
        public TypeTask TypeTask { get; set; }

        public int? UserJobId { get; set; }

        [Display(Name = "File")]
        public string filePath { get; set; }

        [Display(Name = "File")]
        public HttpPostedFileBase file { get; set; }

        [Display(Name = "Fecha de registro")]
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime RegistrationDate { get; set; }
    }
}