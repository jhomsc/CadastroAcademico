﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace CadastroAcademico.Models
{
    public class Course
    {
        [Key]
        public int Id { get; set; } 

        [Required(ErrorMessage = "The field {0} is required")]
        [StringLength(100, ErrorMessage = "The field {0} can contain maximun {1} and minimun {2} characters", MinimumLength = 5)]
        [Display(Name = "Nombre del Curso")]
        public string Name { get; set; }

        [Required(ErrorMessage = "The field {0} is required")]
        [StringLength(1000, ErrorMessage = "The field {0} can contain maximun {1} and minimun {2} characters", MinimumLength = 5)]
        [Display(Name = "Descripción del Curso")]
        public string Description { get; set; }

        [Display(Name = "Activo")]
        public bool Active { get; set; }

        [Required(ErrorMessage = "The field {0} is required")]
        [Display(Name = "Fecha de creacion")]
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime CreationDate { get; set; }


        public virtual ICollection<Registration> Registrations { get; set; }
        public virtual ICollection<Matter> Matters { get; set; }
    }
}