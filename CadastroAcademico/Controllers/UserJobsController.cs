﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CadastroAcademico.Models;

namespace CadastroAcademico.Controllers
{
    [Authorize(Roles = "Professor")]
    public class UserJobsController : Controller
    {
        private CadastroAcademicoContext db = new CadastroAcademicoContext();

        // GET: UserJobs
        public ActionResult Index()
        {
            var user = db.Users.Where(u => u.UserName.ToLower().Equals(User.Identity.Name)).FirstOrDefault();
            var userJobs = db.UserJobs
                .Include(u => u.Job)
                .Include(u => u.User)
                .Include(u => u.Job.Matter)
                .Include(u => u.Job.Matter.Course)
                .Where(s => s.Job.Matter.UserId.Equals(user.UserId));

            return View(userJobs.ToList());
        }



        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
