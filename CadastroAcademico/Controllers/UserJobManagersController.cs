﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CadastroAcademico.Models;
using System.IO;

namespace CadastroAcademico.Controllers
{
    [Authorize(Roles = "Usuario")]
    public class UserJobManagersController : Controller
    {
        private CadastroAcademicoContext db = new CadastroAcademicoContext();
        // GET: UserJobManager
        public ActionResult Index()
        {
            var user = db.Users.Where(u => u.UserName.ToLower().Equals(User.Identity.Name)).FirstOrDefault();

            var courses =
                from Registration in db.Registrations
                where Registration.UserId == user.UserId
                select Registration.CourseId;

            var jobs = db.Jobs.Include(j => j.Matter)
                .Include(c => c.Matter.Course)
                .Include(r => r.UserJobs)
                .Where(s => courses.Contains(s.Matter.Course.Id) && (s.Matter.Course.Active==true));
//                .Where(s => courses.Contains(s.Matter.Course.Id) && (s.Matter.Course.Active == true) && ((DateTime.Now >= s.BeginDate) && (DateTime.Now <= s.EndDate)));

            var userJobManagers = new List<UserJobManagerView>();
            foreach (var item in jobs)
            {
                var userJobManagerView = new UserJobManagerView
                {
                    CourseName =item.Matter.Course.Description,
                    JobBeginDate = item.BeginDate,
                    JobDescription = item.Description,
                    JobEndDate = item.EndDate,
                    JobId = item.Id,
                    JobName = item.Name,
                    MatterName = item.Matter.Name,
                    TypeTask = item.TypeTask,
                    //UserJobId = "",
                    //file = =item.UserJobs,
                    //RegistrationDate = "",
                };

                var userJob = db.UserJobs.Where(u => u.UserId == user.UserId && u.JobId == item.Id).FirstOrDefault();

                if (userJob != null)
                {

                    userJobManagerView.UserJobId = userJob.Id;
                    userJobManagerView.filePath = userJob.file;
                    userJobManagerView.RegistrationDate = userJob.RegistrationDate;
                }


                userJobManagers.Add(userJobManagerView);
            }



            return View(userJobManagers);
            
        }

        // GET: Jobs/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Job job = db.Jobs.Find(id);

                if (job == null)
            {
                return HttpNotFound();
            }

            var user = db.Users.Where(u => u.UserName.ToLower().Equals(User.Identity.Name)).FirstOrDefault();

            var userJobManagerView = new UserJobManagerView
            {
                CourseName = job.Matter.Course.Description,
                JobBeginDate = job.BeginDate,
                JobDescription = job.Description,
                JobEndDate = job.EndDate,
                JobId = job.Id,
                JobName = job.Name,
                MatterName = job.Matter.Name,
                TypeTask = job.TypeTask,
                //UserJobId = "",
                //file = =item.UserJobs,
                //RegistrationDate = "",
            };

            var userJob = db.UserJobs.Where(u => u.UserId == user.UserId && u.JobId == job.Id).FirstOrDefault();

            if (userJob != null)
            {

                userJobManagerView.UserJobId = userJob.Id;
                userJobManagerView.filePath = userJob.file;
                userJobManagerView.RegistrationDate = userJob.RegistrationDate;
            }

            return View(userJobManagerView);
        }

        // POST: Jobs/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(UserJobManagerView userJobManagerView)
        {
            if (!ModelState.IsValid)
            {
                return View(userJobManagerView);
            }

            if ((DateTime.Now >= userJobManagerView.JobBeginDate) && (DateTime.Now <= userJobManagerView.JobEndDate))
            {

                //upload image
                string path = string.Empty;
                string pic = string.Empty;
                var user = db.Users.Where(u => u.UserName.ToLower().Equals(User.Identity.Name)).FirstOrDefault();

                if (userJobManagerView.file != null)
                {
                    path = Path.Combine(Server.MapPath("~/App_Data/Jobs"), user.UserId.ToString());
                    if (!Directory.Exists(path))
                        System.IO.Directory.CreateDirectory(path);

                    path = Path.Combine(Server.MapPath("~/App_Data/Jobs"), user.UserId.ToString(), userJobManagerView.JobId.ToString());
                    if (!System.IO.Directory.Exists(path))
                        System.IO.Directory.CreateDirectory(path);

                    path = Server.MapPath(userJobManagerView.filePath);
                    if (System.IO.File.Exists(path))
                        System.IO.File.Delete(path);


                    pic = Path.GetFileName(userJobManagerView.file.FileName);
                    path = Path.Combine(Server.MapPath("~/App_Data/Jobs"), user.UserId.ToString(), userJobManagerView.JobId.ToString(), pic);
                    userJobManagerView.file.SaveAs(path);
                    using (MemoryStream ms = new MemoryStream())
                    {
                        userJobManagerView.file.InputStream.CopyTo(ms);
                        byte[] array = ms.GetBuffer();
                    }
                }

                //Si existe se elimina
                if (userJobManagerView.UserJobId != null)
                {

                    UserJob uJob = db.UserJobs.Find(userJobManagerView.UserJobId);
                    db.UserJobs.Remove(uJob);
                    try
                    {
                        db.SaveChanges();
                    }
                    catch (Exception ex)
                    {
                        if (ex.InnerException != null &&
                            ex.InnerException.InnerException != null &&
                            ex.InnerException.InnerException.Message.Contains("REFERENCE"))
                        {
                            ViewBag.Error = "Can't delete the record, because exist record related";
                        }
                        else
                        {
                            ViewBag.Error = ex.Message;
                        }
                        return View(userJobManagerView);
                    }
                    //Eliminar el fichero fisico
                    //....
                }
                //Se crea nuevo

                var userJob = new UserJob
                {
                    file = pic == string.Empty ? string.Empty : string.Format("~/App_Data/Jobs/{0}/{1}/{2}", user.UserId.ToString(), userJobManagerView.JobId.ToString(), pic),
                    RegistrationDate = DateTime.Now,
                    UserId = user.UserId,
                    JobId = userJobManagerView.JobId

                };

                db.UserJobs.Add(userJob);
                try
                {
                    db.SaveChanges();
                }
                catch (Exception ex)
                {
                    ViewBag.Error = ex.Message;
                    return View(userJobManagerView);
                }
            }
            else
            {
                ViewBag.Error = "No estas en el periodo de entrega";
                return View(userJobManagerView);

            }
            return RedirectToAction("Index");
        }

        private void CreateDirectory(string v)
        {
            throw new NotImplementedException();
        }
    }
}