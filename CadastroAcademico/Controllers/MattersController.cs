﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CadastroAcademico.Models;

namespace CadastroAcademico.Controllers
{
    [Authorize(Roles = "Administrador,Professor")]
    public class MattersController : Controller
    {
        private CadastroAcademicoContext db = new CadastroAcademicoContext();

        // GET: Matters
        public ActionResult Index()
        {
            var matters = db.Matters.Include(m => m.Course).Include(m => m.User);
            return View(matters.ToList());
        }

        // GET: Matters/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Matter matter = db.Matters.Find(id);
            if (matter == null)
            {
                return HttpNotFound();
            }
            return View(matter);
        }

        // GET: Matters/Create
        public ActionResult Create()
        {
            ViewBag.CourseId = new SelectList(db.Courses.Where(u=>u.Active==true), "Id", "Name");
            ViewBag.UserId = new SelectList(db.Users.Where(u => u.TypeUser == TypeUser.Professor).ToList(), "UserId", "UserName");
            return View();
        }

        // POST: Matters/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Name,Description,UserId,CourseId")] Matter matter)
        {
            if (ModelState.IsValid)
            {
                db.Matters.Add(matter);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.CourseId = new SelectList(db.Courses.Where(u => u.Active == true), "Id", "Name", matter.CourseId);
            ViewBag.UserId = new SelectList(db.Users.Where(u => u.TypeUser== TypeUser.Professor).ToList(), "UserId", "UserName", matter.UserId);
            return View(matter);
        }

        // GET: Matters/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Matter matter = db.Matters.Find(id);
            if (matter == null)
            {
                return HttpNotFound();
            }
            ViewBag.CourseId = new SelectList(db.Courses, "Id", "Name", matter.CourseId);
            ViewBag.UserId = new SelectList(db.Users.Where(u => u.TypeUser == TypeUser.Professor).ToList(), "UserId", "UserName", matter.UserId);
            return View(matter);
        }

        // POST: Matters/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Name,Description,UserId,CourseId")] Matter matter)
        {
            if (ModelState.IsValid)
            {
                db.Entry(matter).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.CourseId = new SelectList(db.Courses, "Id", "Name", matter.CourseId);
            ViewBag.UserId = new SelectList(db.Users.Where(u => u.TypeUser == TypeUser.Professor).ToList(), "UserId", "UserName", matter.UserId);
            return View(matter);
        }

        // GET: Matters/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Matter matter = db.Matters.Find(id);
            if (matter == null)
            {
                return HttpNotFound();
            }
            return View(matter);
        }

        // POST: Matters/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Matter matter = db.Matters.Find(id);
            db.Matters.Remove(matter);
            try
            {
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                if (ex.InnerException != null &&
                    ex.InnerException.InnerException != null &&
                    ex.InnerException.InnerException.Message.Contains("REFERENCE"))
                {
                    ModelState.AddModelError("", "Can't delete the record, because exist record related");
                }
                else
                {
                    ModelState.AddModelError("", ex.Message);
                }
                return View(matter);
            }
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
