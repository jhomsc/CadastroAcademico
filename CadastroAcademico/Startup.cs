﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(CadastroAcademico.Startup))]
namespace CadastroAcademico
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
